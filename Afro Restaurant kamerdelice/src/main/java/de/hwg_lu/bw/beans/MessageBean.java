package de.hwg_lu.bw.beans;

public class MessageBean {

	String infoMessage;
	String actionMessage;
	
	public MessageBean() {
//		this.infoMessage = "Willkommen an der Webanwendung BWI420";
//		this.actionMessage = "Bitte tun Sie irgendwas";
		  setRegistrationWelcome();
	}
	public MessageBean(String infoMessage, String actionMessage) {
		super();
		this.infoMessage = infoMessage;
		this.actionMessage = actionMessage;
	}
	public void setRegistrationWelcome(){
		this.infoMessage = "";
		this.actionMessage = "Bitte geben Sie Ihre Daten ein";
	}
	
	public void setLogoutSuccessful(){
		this.infoMessage = "Sie haben sich erfolgreich abgemeldet";
		this.actionMessage = "Bitte melden Sie sich wieder an";
	}
	
	public void setLoginFailed(){
		this.infoMessage = "Ihr Anmeldeversuch ist fehlgeschlagen";
		this.actionMessage = "Bitte versuchen Sie es noch einmal";
	}
	
	public String getMessageHtml(){
		String html = "";
		html += "<p style=\"color:red\">" + this.getInfoMessage() + "<br>";
		html += "" + this.getActionMessage() + "</p>";
		return html;
	}
	
	public void setAccountCreated(String userid){
		this.setInfoMessage("Account " + userid + " wurde angelegt");
		this.setActionMessage("Geben Sie jetzt Ihre Daten zur Anmeldung");
	}
	public void setReservierungCreated(String id){
		this.setInfoMessage("Reservierung " + id + " wurde angelegt");
		this.setActionMessage("Gehen Sie jetzt zur Übersicht");
	}
	public void setAccountAlreadyExists(String userid){
		this.setInfoMessage("Nickname " + userid + " existiert bereits");
		this.setActionMessage("Waehlen Sie einen anderen Nickname");
	}
	
	public void setDBError(){
		this.setInfoMessage("Die Verbindung konnte nicht hergestellt werden");
		this.setActionMessage("Wenden Sie sich an IhreN AdministratorIn");
	}
	public void setSQLError(){
		this.setInfoMessage("Es ist ein Datenbankfehler aufgetreten");
		this.setActionMessage("Wenden Sie sich an IhreN AdministratorIn");
	}
	 
	public String getInfoMessage() {
		return infoMessage;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	public String getActionMessage() {
		return actionMessage;
	}
	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}
}
