package de.hwg_lu.bw.beans;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;

import de.hwg_lu.bw.jdbc.NoConnectionException;
import de.hwg_lu.bw.jdbc.PostgreSQLAccess;

@WebServlet("/api/products")
public class ProductServlet extends HttpServlet {

	/**
	 * api zum bereitstellen der Produkten
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		ProductServlet p = new ProductServlet();
		try {
			p.retrieveProductsFromDatabase();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Product> retrieveProductsFromDatabase() throws SQLException, NoConnectionException {
		List<Product> products = new ArrayList<>();
		// einf�gen von Testprodukte
		products.add(new Product(1, "PRODUCT NAME 1", "1.PNG", 120000, "Apperetif"));
		products.add(new Product(2, "PRODUCT NAME 2", "2.PNG", 120000, "Apperetif"));
		products.add(new Product(3, "PRODUCT NAME 3", "3.PNG", 220000, "Apperetif"));
		products.add(new Product(4, "PRODUCT NAME 4", "4.PNG", 123000, "Apperetif"));
		products.add(new Product(5, "PRODUCT NAME 5", "5.PNG", 320000, "Apperetif"));
		products.add(new Product(6, "PRODUCT NAME 6", "6.PNG", 120000, "Apperetif"));

		Connection connection = new PostgreSQLAccess().getConnection();

		String query = "SELECT id, name,image, price,category FROM products";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		ResultSet resultSet = preparedStatement.executeQuery();

		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name = resultSet.getString("name");
			String image = resultSet.getString("image");
			String category = resultSet.getString("category");
			double price = resultSet.getDouble("price");

			Product product = new Product(id, name, image, price, category);
			products.add(product);
		}
		System.out.println(products.toString());
		return products;
	}

	// Java Servlet, um Daten aus der Datenbank abzurufen und als JSON zu senden
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Rufen der Daten aus der Datenbank ab und formatieren als JSON
		List<Product> products = null;
		try {
			products = retrieveProductsFromDatabase();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Konvertieren der Produktdaten in JSON
		String json = new Gson().toJson(products);

		// Setzen  den Content-Type-Header auf JSON
		response.setContentType("application/json");

		// Schreiben  JSON-Daten in die Antwort
		response.getWriter().write(json);
	}

}
