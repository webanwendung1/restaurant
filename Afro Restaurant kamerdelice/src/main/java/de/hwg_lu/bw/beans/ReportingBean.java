package de.hwg_lu.bw.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.hwg_lu.bw.jdbc.PostgreSQLAccess;

public class ReportingBean {

	int umsatz;
	int AnzahlPendingBestellungen;
	String Kundenname;
	String status;
	String Verkaufsdatum;
	String reservations;
	int Gesamtumsatz;
	int anzahlKunde;
	
	
	public ReportingBean() throws SQLException {
		// TODO Auto-generated constructor stub
		getKennzalFromDB();
	}
	
	public String getKennzalFromDB() throws SQLException {
		// TODO Auto-generated method stub
		Connection conn= new PostgreSQLAccess().getConnection();
		String html="";
				String sql1_bestellunge = ""
						+ "SELECT\r\n"
						+ "    SUM(w.quantity * p.price) as kundenumsatz,\r\n"
						+ "    a.name as Kundenname,\r\n"
						+ "    w.erstellt_am as Verkaufsdatum,\r\n"
						+ "    w.status\r\n"
						+ "FROM\r\n"
						+ "    bwi521_634768.warenkorb as w\r\n"
						+ "JOIN\r\n"
						+ "    bwi521_634768.products p   ON w.produkt_id = p.id\r\n"
						+ "JOIN\r\n"
						+ "    bwi521_634768.account as a ON w.user_id = a.userid\r\n"
						+ "GROUP BY\r\n"
						+ "    w.status,a.name, w.erstellt_am;\r\n"
						+ "   ";
				
				
				String sql2_gesamtumsatz = "  SELECT SUM(w.quantity * p.price) AS Gesamtumsatz\r\n"
						+ "FROM bwi521_634768.warenkorb AS w\r\n"
						+ "JOIN bwi521_634768.products AS p ON w.produkt_id = p.id;\r\n"
						+ "	";
				
				
				String sql3_AnzahlKunden = "select count('userid') as anzahlKunde from bwi521_634768.account a\r\n"
						+ "where a.admin ='N';";
				
				String sql4_Anzahlbestelung = "select count(*) as AnzahlPendingBestellungen from bwi521_634768.warenkorb;";
				
				String sql5_Reservations = "SELECT reservationid, heur, \"date\", message, nbpers, email, \"name\" FROM bwi521_634768.reservation;";
				
				PreparedStatement prep1= conn.prepareStatement(sql1_bestellunge);
				PreparedStatement prep2= conn.prepareStatement(sql2_gesamtumsatz);
				PreparedStatement prep3= conn.prepareStatement(sql3_AnzahlKunden);
				PreparedStatement prep4= conn.prepareStatement(sql4_Anzahlbestelung);
				PreparedStatement prep5= conn.prepareStatement(sql5_Reservations);
				
				ResultSet res1=prep1.executeQuery();
				while (res1.next()) {
					 umsatz= res1.getInt("kundenumsatz");
					 Kundenname=res1.getString("Kundenname");
					 status=res1.getString("status");
					// String fulldate=substring(0, 9);
					 Verkaufsdatum = res1.getString("Verkaufsdatum").substring(0, 9);
					 
					 html+=""
					 		+ "<tr>\r\n"
					 		+ "<td>\r\n"
					 		+ "<img src=\"../img/people.jpg\">\r\n"
					 		+ "<p>"+Kundenname+"</p>\r\n"
					 		+ "</td>\r\n"
					 		+ "<td>"+Verkaufsdatum+"</td>\r\n"
					 		+ "<td>$"+umsatz+"</td>\r\n"
					 		+ "<td><span class=\"status "+status+"\">"+status+"</span></td>\r\n"
					 		+ "</tr>"
					 		+ "";
				}
				ResultSet res2=prep2.executeQuery();
				while (res2.next()) {
					Gesamtumsatz= res2.getInt("Gesamtumsatz");
					
				}
				ResultSet res3=prep3.executeQuery();
				while (res3.next()) {
					anzahlKunde= res3.getInt("anzahlKunde");
					
				}
				ResultSet res4=prep4.executeQuery();
				while (res4.next()) {
					 AnzahlPendingBestellungen=res4.getInt("AnzahlPendingBestellungen");					
				}
				ResultSet res5=prep5.executeQuery();
				reservations="";
				while (res5.next()) {
					reservations+=""
							+ "<tr>\r\n"
							+ "<td>\r\n"
							+ "<p>"+res5.getString("name")+"</p>\r\n"
							+ "</td>\r\n"
							+ "<td>"+res5.getString("date")+"</td>\r\n"
							+ "<td>"+res5.getString("nbpers")+"</td>\r\n"
							+ "<td>"+res5.getString("heur")+"</td>\r\n"
							+ "</tr>"
							+ "";
				}
				
				return html;
				
				
	}

	 // Methode zum Umkehren eines Strings in Java mit `StringBuilder`
    public static String reverse(String str) {
        return new StringBuilder(str).reverse().toString();
    }
	/**
	 * @return the umsatz
	 */
	public int getUmsatz() {
		return umsatz;
	}

	/**
	 * @param umsatz the umsatz to set
	 */
	public void setUmsatz(int umsatz) {
		this.umsatz = umsatz;
	}

	/**
	 * @return the anzahlPendingBestellungen
	 */
	public int getAnzahlPendingBestellungen() {
		return AnzahlPendingBestellungen;
	}

	/**
	 * @param anzahlPendingBestellungen the anzahlPendingBestellungen to set
	 */
	public void setAnzahlPendingBestellungen(int anzahlPendingBestellungen) {
		AnzahlPendingBestellungen = anzahlPendingBestellungen;
	}

	/**
	 * @return the kundenname
	 */
	public String getKundenname() {
		return Kundenname;
	}

	/**
	 * @param kundenname the kundenname to set
	 */
	public void setKundenname(String kundenname) {
		Kundenname = kundenname;
	}

	/**
	 * @return the verkaufsdatum
	 */
	public String getVerkaufsdatum() {
		return Verkaufsdatum;
	}

	/**
	 * @param verkaufsdatum the verkaufsdatum to set
	 */
	public void setVerkaufsdatum(String verkaufsdatum) {
		Verkaufsdatum = verkaufsdatum;
	}

	/**
	 * @return the gesamtumsatz
	 */
	public int getGesamtumsatz() {
		return Gesamtumsatz;
	}

	/**
	 * @param gesamtumsatz the gesamtumsatz to set
	 */
	public void setGesamtumsatz(int gesamtumsatz) {
		Gesamtumsatz = gesamtumsatz;
	}

	/**
	 * @return the anzahlKunde
	 */
	public int getAnzahlKunde() {
		return anzahlKunde;
	}

	/**
	 * @param anzahlKunde the anzahlKunde to set
	 */
	public void setAnzahlKunde(int anzahlKunde) {
		this.anzahlKunde = anzahlKunde;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the reservations
	 */
	public String getReservations() {
		return reservations;
	}

	/**
	 * @param reservations the reservations to set
	 */
	public void setReservations(String reservations) {
		this.reservations = reservations;
	}
	
	
	
	
}
