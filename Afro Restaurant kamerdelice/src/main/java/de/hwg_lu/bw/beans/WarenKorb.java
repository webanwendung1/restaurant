package de.hwg_lu.bw.beans;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import de.hwg_lu.bw.jdbc.PostgreSQLAccess;

	public class WarenKorb {
	    private Map<Integer, Integer> selectedProdukte;
	    private int userId;
	    public WarenKorb() {
	        selectedProdukte = new HashMap<>();
	    }

	    public void addProdukt(int produktID, int quantity) {
	        // Wenn das Produkt bereits im Warenkorb ist, erh�he die Menge, andernfalls f�ge es hinzu.
	        selectedProdukte.put(produktID, selectedProdukte.getOrDefault(produktID, 0) + quantity);
	    }

	    public void saveToDatabase() throws SQLException {
	    	Connection dbConn = new PostgreSQLAccess().getConnection();
			
			
			for (Integer itemId : selectedProdukte.keySet()) {
				String sql="INSERT INTO bwi521_634768.warenkorb (user_id, produkt_id, quantity)\r\n"
						+ "VALUES (?, ?, ?);";
				PreparedStatement prep = dbConn.prepareStatement(sql);
				prep.setInt(1, userId);
				prep.setInt(2, itemId);
				prep.setInt(3, selectedProdukte.get(itemId));
				prep.executeUpdate();
				System.out.println("item mit id "+ itemId+"eingef�gt");
			}
			
			System.out.println("alle selected item eingef�gt");
	    }
	    
	    public void setUserId(int userId) {
			System.out.println(userId+" id recieved von der loginbean");
			this.userId = userId;
		}
	}
	
