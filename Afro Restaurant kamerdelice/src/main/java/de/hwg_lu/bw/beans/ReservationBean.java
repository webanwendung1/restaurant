package de.hwg_lu.bw.beans;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import de.hwg_lu.bw.jdbc.NoConnectionException;
import de.hwg_lu.bw.jdbc.PostgreSQLAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;




public class ReservationBean {

	String name = "K.A.";
	String mail ="sophiealida21@gmail.com";
	String date = "K.A.";
	String heur ="K.A.";
	String nb_personne = "K.A.";
	String message = "K.A.";
	String phone = "K.A.";
	static int zaehler=0;
	boolean reservationSearh=false;
	int id=0;
	int SuchUndfound=0;
	//recherche des reservation
	String emailRecher ="";
	int idRecher=-1;
	String nomRecher="";
	
	
	Reservation reservZeiger=new Reservation( name, mail,  date,  heur,  nb_personne,  message,  id);

	int modifAnnullID;

     Session session;

	ArrayList<Reservation> reservations = new ArrayList<Reservation>();
	ArrayList<Reservation> rechercheList = new ArrayList<Reservation>();
	
//	// pour les testes du programme..
    public static void main(String[] args) throws SQLException {
    	ReservationBean rb =new ReservationBean();
    	EmailSender es= new EmailSender();
    	
    	// es.sendMail();
	}
	public ReservationBean() throws SQLException {
		
		 // D�finir les propri�t�s du serveur SMTP
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");
             // Cr�er une session d'authentification avec le serveur SMTP
         session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        
            getAllReservation();
		reservations.add(new Reservation( "Sophie Alida", "Sophie@mail.com",  "06.05.2023",  "22:30",  "2",  " Wir m�chten ... essen!",  0));
		rechercheList.add(new Reservation( "Sophie Jonfang","Sophie@mail.com",  "06.03.2023",  "22:30",  "1",  " okok",  1));
		reservations.add(new Reservation( "Sophie Alida", "Sophie@mail.com",  "05.03.2023",  "22:30",  "2",  " Wir m�chten ... essen!",  2));
		reservations.add(new Reservation( "Sophie Alida", "Sophie@mail.com",  "07.03.2023",  "22:30",  "8",  " Wir m�chten ... essen!",  3));
		reservations.add(new Reservation( "Sophie Alida", "Sophie@mail.com",  "06.03.2023",  "22:30",  "2",  " Wir m�chten ... essen!",  4));
		reservations.add(new Reservation( "Sophie Alida", "Sophie@mail.com",  "02.08.2023",  "22:30",  "2",  " Wir m�chten ... essen!",  123));
		reservations.add(new Reservation( "Sophie Alida", "Sophie@mail.com",  "06.03.2023",  "22:30",  "2",  " Wir m�chten ... essen!",  789));
		reservations.add(new Reservation( "Sophie Alida", "Sophie@mail.com",  "06.07.2023",  "22:30",  "12",  " Wir m�chten ... essen!",  456));

	}

	public void createReservation() throws SQLException {
		
		reservations.clear();
		Connection dbConn = new PostgreSQLAccess().getConnection();
		String sql="INSERT INTO bwi521_634768.reservation (heur, \"date\", "
				+ "message, nbpers, email, \"name\") VALUES(?, ?, ?, ?, ?, ?);\r\n"
				+ "";
		PreparedStatement prep = dbConn.prepareStatement(sql);
		
		prep.setString(1, heur);
		prep.setString(2, date);
		prep.setString(3, message);
		prep.setString(4, nb_personne);
		prep.setString(5, mail);
		prep.setString(6, name);
		prep.executeUpdate();
		//zu mail senden
		reservZeiger=new Reservation( name, mail,  date,  heur,  nb_personne,  message,  id);
		System.out.println("reservirung insertet");
		confirmReservationSendMail("Bestaetigung Ihrer Reservierung");
		getAllReservation();
	}
	public void getAllReservation() throws SQLException {
		reservations.clear();
		Connection dbConn = new PostgreSQLAccess().getConnection();
		String sql="SELECT reservationid, heur, \"date\", message, nbpers, email, \"name\""
				+ " FROM bwi521_634768.reservation;\r\n"
				+ "";
		PreparedStatement prep = dbConn.prepareStatement(sql);
		ResultSet res =prep.executeQuery();
		while (res.next()) {
			String name=res.getString("name");
			String mail=res.getString("email");
			String date=res.getString("date");
			String heur=res.getString("heur");
			String nb_personne=res.getString("nbpers");
			int id=res.getInt("reservationid");
			String message=res.getString("message");
			zaehler=id;
			reservations.add(new Reservation( name, mail,  date,  heur,  nb_personne,  message,  id));

		}
		
		
		
	}
	public void confirmReservationSendMail(String subjet) {
		
		
		EmailSender es= new EmailSender();
		es.setBodyMail("Sehr geehrte Damen und Herren,\r\n"
				+ "\r\n"
				+ "wir freuen uns, Ihre Reservierung im Restaurant KamerDelices zu bestaetigen. Die Details Ihrer Reservierung lauten wie folgt:\r\n"
				+ "\r\n"
				+ "Reservirungsnummer: "+reservZeiger.getId()+"\r\n"
				+ "Name: "+reservZeiger.getName()+"\r\n"
				+ "Datum: "+reservZeiger.getDate()+"\r\n"
				+ "Uhrzeit: "+ reservZeiger.getHeur()+"\r\n"
				+ "Anzahl der Personen: "+ reservZeiger.getNb_personne()+"\r\n"
				+ "\r\n"
				+ "Wir haben auch Ihre speziellen Wuensche beruecksichtigt:\r\n"
				+ "\r\n"
				+ ""+ reservZeiger.getMessage()+"\r\n"
				+ "\r\n"
				+ "Wir bedanken uns fuer Ihr Vertrauen und freuen uns darauf, Sie in unserem Restaurant begruessen zu duerfen.\r\n"
				+ "\r\n"
				+ "Mit freundlichen Gruessen,\r\n"
				+ "\r\n"
				+ "Das Team des Restaurants KamerDelices");
		
		es.setSujet(subjet);
		es.setTo(getMail());
		
   	    es.sendMail();
		
   	
	}
	public void recherche() throws SQLException {
		getAllReservation();
		SuchUndfound=1;
		System.out.println("suche");
		for (Reservation reserv : reservations) {
			
			if ((reserv.getId()==getIdRecher()&&
					reserv.getMail().toUpperCase().equals(getEmailRecher().toUpperCase()))
					|| (reserv.getMail().toUpperCase().equals(getEmailRecher().toUpperCase())
							&& reserv.getName().toUpperCase().equals(getNomRecher().toUpperCase()))) {
				
				rechercheList.add(reserv);
				System.out.println("1 match");
				SuchUndfound=2;
			}
		}
	}
	
	public void annullerReservation(String id) throws SQLException {
		int idInt = Integer.parseInt(id);
		
		reservations.clear();
		Connection dbConn = new PostgreSQLAccess().getConnection();
		String sql="DELETE FROM bwi521_634768.reservation WHERE reservationid="+idInt+";\r\n";
		PreparedStatement prep = dbConn.prepareStatement(sql);
		
		prep.executeUpdate();

		 
		getAllReservation();
		
		 
		confirmReservationSendMail("Stornirung Ihrer Reservierung");
	}
	
	public void modificationReservation() throws SQLException {
		
		
		Connection dbConn = new PostgreSQLAccess().getConnection();
		String sql="UPDATE bwi521_634768.reservation SET heur='"+getHeur()+"', \"date\"='"+getDate()+"', message='"+getMessage()+"'::bpchar, nbpers='"+getNb_personne()+"', email='"+getMail()+"', \"name\"='"+getName()+"' WHERE reservationid="+modifAnnullID+";\r\n"
				+ "";
		PreparedStatement prep = dbConn.prepareStatement(sql);
		prep.executeUpdate();
		confirmReservationSendMail("Aenderungsbestaetigung Ihrer Reservierung");

		reservations.clear();
		 
		getAllReservation();
		System.out.println("annuller id " +modifAnnullID);
		
		for (Reservation reservationModif : reservations) {
			
			if ((reservationModif.getId()==modifAnnullID)){
				
				reservationModif.setName(getName());
				reservationModif.setDate(getDate());
				reservationModif.setHeur(getHeur());
				reservationModif.setMessage(getMessage());
				reservationModif.setNb_personne(getNb_personne());
				reservZeiger=reservationModif;
				
				
			}	}	
	}
	public void setReservationZeiger(String id) {
		int idInt = Integer.parseInt(id);
		
		System.out.println("annuller id " +idInt);
		
		for (Reservation reservationModif : reservations) {
			
			if ((reservationModif.getId()==idInt)){
				
				
				reservZeiger=reservationModif;
				// f�r die getProperty
				setName( reservZeiger.getName());
				setDate( reservZeiger.getDate());
				setHeur( reservZeiger.getHeur());
				setMessage( reservZeiger.getMessage());
				setNb_personne( reservZeiger.getNb_personne());
				
				//confirmReservationSendMail("Aenderungsbestaetigung Ihrer Reservierung");
			}	}	
	}
	
	public String getResultset(){
		if (!reservationSearh) 
			return "";
		
		String html=""
				+ "<h2>Search Results</h2>\r\n"
				
				+ "<thead>\r\n"
				+ "		        <tr>\r\n"
				+ "		            <th class=\"none\">ID</th>\r\n"
				+ "		            <th>Customer Name</th>\r\n"
				+ "		            <th>Date</th>\r\n"
				+ "		            <th>Time</th>\r\n"
				+ "		            <th class=\"none\">Number of People</th>\r\n"
				+ "		            <th class=\"none\">Special Requests</th>\r\n"
				+ "		            <th>Action</th>\r\n"
				+ "		        </tr>\r\n"
				+ "	</thead>\r\n"
				+ "	<tbody>"
				+ "";
		for (Reservation currentReservation : rechercheList) {
			
			html+=" <tr>\r\n"
					+ "<td class=\"none\">"+ currentReservation.getId()+"</td>\r\n"
					+ "<td>"+currentReservation.getName()+"</td>\r\n"
					+ "<td>"+currentReservation.getDate()+"</td>\r\n"
					+ "<td>"+currentReservation.getHeur()+"</td>\r\n"
					+ "<td class=\"none\">"+currentReservation.getNb_personne()+"</td>\r\n"
					+ "<td class=\"none\">"+currentReservation.getMessage()+"</td>\r\n"
					+ "<td>\r\n"
					+ "  <a href=\"reservationAppl.jsp?modification=Modifier&idModif="+currentReservation.getId()+"\" class=\"btn btn-sm btn-warning\" style=\"background-color:black;\">Modifier</a>\r\n"
					+ "  <a href=\"reservationAppl.jsp?annullation=Annuller&idAnnul="+currentReservation.getId()+"\"\" class=\"btn btn-sm btn-danger\">Annuler </a>\r\n"
					+ "</td>\r\n"
					+ "</tr>";
		
		}
		
		return html;
	}
	public String getResultNotFound(){
		if (SuchUndfound==2) {
			return "Voila toutes vos reservations";
		}else if (SuchUndfound==1) {
			return "Desole, accune reservation n a ete trouve";
		}
		return"";
		
	}
	
	 public void sendMail() {
	        try {
	            // Cr�er un message MIME
	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(getFrom()));
	            message.setRecipients(Message.RecipientType.TO,
	                    InternetAddress.parse(getTo()));
	            message.setRecipients(Message.RecipientType.CC,
	                    InternetAddress.parse(getCc()));
	            message.setSubject(getSujet());
	            message.setText(getBodyMail());

	            // Envoyer le message
	            Transport.send(message);

	            System.out.println("Le message a �t� envoy� avec succ�s.");

	        } catch (MessagingException e) {
	            throw new RuntimeException(e);
	        }
	    }

		/**
		 * @return the to
		 */
		public String getTo() {
			return to;
		}

		/**
		 * @param to the to to set
		 */
		public void setTo(String to) {
			this.to = to;
		}
	public String getCc() {
		return cc;
	}
		/**
		 * @return the from
		 */
		public String getFrom() {
			return from;
		}

		/**
		 * @param from the from to set
		 */
		public void setFrom(String from) {
			this.from = from;
		}

		/**
		 * @return the sujet
		 */
		public String getSujet() {
			return sujet;
		}

		public void setBodyMail(String bodyMail) {
			this.bodyMail = bodyMail;
		}
		
		public String getBodyMail() {
			return bodyMail;
		}
		/**
		 * @param sujet the sujet to set
		 */
		public void setSujet(String sujet) {
			this.sujet = sujet;
		}
	    
		public void setModifAnnullID(int modifAnnullID) {
			this.modifAnnullID = modifAnnullID;
		}
		public int getModifAnnullID() {
			return modifAnnullID;
		}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
public int getZaehler() {
	return zaehler;
}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
public void setMail(String mail) {
	this.mail = mail;
}
public String getMail() {
	return mail;
}
	/**
	 * @return the heur
	 */
	public String getHeur() {
		return heur;
	}

	/**
	 * @param heur the heur to set
	 */
	public void setHeur(String heur) {
		this.heur = heur;
	}

	/**
	 * @return the nb_personne
	 */
	public String getNb_personne() {
		return nb_personne;
	}

	/**
	 * @param nb_personne the nb_personne to set
	 */
	public void setNb_personne(String nb_personne) {
		this.nb_personne = nb_personne;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the emailRecher
	 */
	public String getEmailRecher() {
		return emailRecher;
	}

	/**
	 * @param emailRecher the emailRecher to set
	 */
	public void setEmailRecher(String emailRecher) {
		this.emailRecher = emailRecher;
	}

	/**
	 * @return the idRecher
	 */
	public int getIdRecher() {
		
		return idRecher;
	}

	/**
	 * @param idRecher the idRecher to set
	 */
	public void setIdRecher(int idRecher) {
		this.idRecher = idRecher;
	}

	/**
	 * @return the nomRecher
	 */
	public String getNomRecher() {
		return nomRecher;
	}

	/**
	 * @param nomRecher the nomRecher to set
	 */
	public void setNomRecher(String nomRecher) {
		this.nomRecher = nomRecher;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
public void setReservationSearh(boolean reservationSearh) {
	this.reservationSearh = reservationSearh;
}
     

}
