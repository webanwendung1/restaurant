package de.hwg_lu.bw.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.hwg_lu.bw.jdbc.PostgreSQLAccess;

/**
 * diese klasse dient deint der verwalltung von logging mit den Passwort und E-mail der Benutzern
 * @author sophie
 *
 */
public class LoginBean {

	String email;
	String password;
	String username;
	int  user_id;
	boolean isLoggedIn=false;
	boolean isAdmin=false;
	boolean LoginSchop=false;
	boolean loginFuerAdmin=false;
	
	/**
	 * set default userId dieferent from a default value
	 */
	public LoginBean() {
		this.email = "nx";
	
	}
	
	/**
	 * @return TRUE OR FALSE  je nachdem ob der benutzer existiert oder nicht
	 * @throws SQLException
	 */
	public boolean checkUseridPassword() throws SQLException {
		String sql = "SELECT  userid , password  from account where email = ? and password = ?";
		System.out.println(sql);
		Connection dbConn = new PostgreSQLAccess().getConnection();
		PreparedStatement prep = dbConn.prepareStatement(sql);
		prep.setString(1, this.email);
		prep.setString(2, this.password);
		ResultSet dbRes = prep.executeQuery();
		 boolean found=dbRes.next();
		 this.user_id= dbRes.getInt("userid");
		 System.out.println(user_id+" ist angemeldet");
		return found;
	}
	
	
	
	
	/**
	 * Ist der Aktuelle benutzer ein Administrator
	 * @author saida leukam
	 *
	 */
	public boolean getAdmin() throws SQLException {
		//wenn man eingelogt ist und admin dann kann man tutoren und accounts verwalten
		String sql = "SELECT userid, password,name from account where email = ? and password = ? and admin= ?";
		System.out.println(sql);
		Connection dbConn = new PostgreSQLAccess().getConnection();
		PreparedStatement prep = dbConn.prepareStatement(sql);
		prep.setString(1, this.getMail());
		prep.setString(2, this.password);
		prep.setString(3, "Y");
		ResultSet dbRes = prep.executeQuery();
		boolean found=dbRes.next();
		user_id= dbRes.getInt("userid");
		username= dbRes.getString("name");
		return found;
		
		
	}
	
	
	/**
	 * @return return the if Admin
	 */
	public boolean isAdmin() {
		return isAdmin;
	}
	public int getUser_id() {
		System.out.println(user_id+"zum warenkorb übermittelt");
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * @param isAdmin set admin status
	 */
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	/**
	 * @return userId return the userId
	 */
	public String getMail() {
		return email;
	}
	/**
	 * @param userid set the userId
	 */
	public void setMail(String userid) {
		this.email = userid;
	}
	/**
	 * @return passwort return the pass
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return loggin retun the Status
	 */
	public boolean isLoggedIn() {
		return isLoggedIn;
	}
	/**
	 * @param isLoggedIn set logging status
	 */
	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	/**
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	public String getLoginStatus() {
		return isLoggedIn==true?"Logout":"Login";
	}

	/**
	 * @param username set username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isLoginSchop() {
		return LoginSchop;
	}
	public void setLoginSchop(boolean loginSchop) {
		LoginSchop = loginSchop;
	}
	public boolean isLoginFuerAdmin() {
		return LoginSchop;
	}
	public void setLoginFuerAdmin(boolean loginSchop) {
		LoginSchop = loginSchop;
	}
}
