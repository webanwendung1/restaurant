let selectedItems = {}; // Variable zur Speicherung ausgewählter Artikel und Mengen

let openShopping = document.querySelector('.shopping');
let closeShopping = document.querySelector('.closeShopping');

let list = document.querySelector('.list');
let listCard = document.querySelector('.listCard');
let body = document.querySelector('.body');
let total = document.querySelector('.total');
let quantity = document.querySelector('.quantity');

let products = [];
let listCards  = [];
loadData(); // Starten  den Datenabruf von der Api

openShopping.addEventListener('click', () => {
    body.classList.add('active');
});

closeShopping.addEventListener('click', () => {
    body.classList.remove('active');
});

// Laden  die Daten und initialisieren  der App
function loadData() {
    fetch('../../api/products') // Stelle sicher, dass die URL auf Ihre API verweist
        .then(response => response.json())
        .then(data => {
            // Verarbeiten  die empfangenen Daten in Ihrem JavaScript-Code
            products = data;
            initApp();
        })
        .catch(error => {
            console.error('Fehler beim Abrufen von Daten: ' + error);
        });
          console.log(products);
}
// produckten grupieren
function groupProductsByCategory(products) {
    const groupedProducts = {};
    
    products.forEach(product => {
        const category = product.category;
        
        if (!groupedProducts[category]) {
            groupedProducts[category] = [];
        }
        
        groupedProducts[category].push(product);
    });
    
    return groupedProducts;
}

function displayProductsByCategory(groupedProducts) {
    const container = document.querySelector('.list'); //ich Stelle sicher, dass dies der richtige Container ist
    
    for (const category in groupedProducts) {
        const products = groupedProducts[category];
        
        // Erstelle  einen Abschnitt für jede Kategorie
        const categorySection = document.createElement('div');
        categorySection.classList.add('category-section');
        
        // Füge den Kategorienamen als Überschrift hinzu
        const categoryHeader = document.createElement('h1');
        categoryHeader.textContent = category;
        categorySection.appendChild(categoryHeader);
        
        // Füge  Produkte dieser Kategorie hinzu
        products.forEach(product => {
            const productDiv = document.createElement('div');
            productDiv.classList.add('item');
            productDiv.innerHTML = `
                <img src="../assets/images/${product.image}">
                <div class="title">${product.name}</div>
                <div class="price">${product.price.toLocaleString()}</div>
                <button style="width: 100%;background-color: #1C1F25; color:white;" class="btn btn-secondary" value="${product.id}" name="selectedItem" onclick="addToCard(${product.id})">Add To Card</button>`;
            categorySection.appendChild(productDiv);
        });
        
        container.appendChild(categorySection);
    }
}


function initApp() {
    const categories = {}; // Ein Objekt zur Aufbewahrung der Produkte nach Kategorien

    products.forEach((value, key) => {
        // Überprüfen, ob die Kategorie bereits existiert, wenn nicht, erstelle ich sie
        if (!categories[value.category]) {
            categories[value.category] = [];
        }

        // Füge das Produkt zur entsprechenden Kategorie hinzu
        categories[value.category].push(value);
    });

      // Jetzt können wir die Produkte in Kategorien anzeigen
    for (const category in categories) {
        const productsInCategory = categories[category];

        // Erstelle eine Überschrift für die Kategorie
        const categoryHeader = document.createElement('h2');
        categoryHeader.textContent = category;
        list.appendChild(categoryHeader);

        // Erstelle einen Container für die Produkte in dieser Kategorie
        const categoryContainer = document.createElement('div');
        categoryContainer.classList.add('category-section');
        categoryContainer.id = `category-${category}`; // Jeder Kategorie-Container erhält eine eindeutige ID

        // Zeigedie Produkte in dieser Kategorie an
        productsInCategory.forEach((product) => {
            let newDiv = document.createElement('div');
            newDiv.classList.add('item');
            newDiv.innerHTML = `
                <img src="../assets/images/${product.image}">
                <div class="title">${product.name}</div>
                <div class="price">${product.price.toLocaleString()}</div>
                <button style="width: 100%; color:white;" class="btn btn-secondary" value="${product.id}
                " name="selectedItem" onclick="addToCard(${product.id})">Add To Card</button>`;
            categoryContainer.appendChild(newDiv);
        });

        // Füge die Kategorie und ihre Produkte in die Liste ein
        list.appendChild(categoryContainer);
    }
}


function filterByCategory() {
    const selectedCategory = document.getElementById('categorySelect').value;
    
    // Lösche die vorherigen Ergebnisse um eine neue zu laden
    list.innerHTML = '';

    // Rufe die ID des Containers für die ausgewählte Kategorie ab
    const categoryContainerId = `category-${selectedCategory}`;

    // wir Filtern die Produkte nach der ausgewählten Kategorie
    const productsInCategory = selectedCategory === 'all' 
        ? products // Zeigen Sie alle Produkte an, wenn "Alle Kategorien" ausgewählt ist
        : products.filter(product => product.category === selectedCategory);

    // Zeige die gefilterten Produkte in der ausgewählten Kategorie an
    const categoryContainer = document.createElement('div');
    categoryContainer.classList.add('category-section');
    categoryContainer.id = categoryContainerId;

    productsInCategory.forEach((product) => {
        let newDiv = document.createElement('div');
        newDiv.classList.add('item');
        newDiv.innerHTML = `
            <img src="../assets/images/${product.image}">
            <div class="title">${product.name}</div>
            <div class="price">${product.price.toLocaleString()}</div>
            <button style="width: 100%; color:white;" class="btn btn-secondary" value="${product.id}"
             name="selectedItem" onclick="addToCard(${product.id})">Add To Card</button>`;
        categoryContainer.appendChild(newDiv);
    });

    // Füge die ausgewählte Kategorie in die Liste ein
    list.appendChild(categoryContainer);
}







function addToCard(key){
    if(listCards[key] == null){
        // copy product form list to list card
        listCards[key] = JSON.parse(JSON.stringify(products[key]));
        listCards[key].quantity = 1;
    }
    // Füge den ausgewählten Artikel und die Menge zur selectedItems-Variable hinzu
    selectedItems[key] = listCards[key].quantity;
    reloadCard();
}
function reloadCard(){
	 console.log('Reload Card Function Called'); // Debug-Ausgabe
    listCard.innerHTML = '';
    let count = 0;
    let totalPrice = 0;
    listCards.forEach((value, key)=>{
        totalPrice = totalPrice + value.price;
        count = count + value.quantity;
        if(value != null){
            let newDiv = document.createElement('li');
            newDiv.innerHTML = `
                <div><img src="../assets/images/${value.image}"/></div>
                <div>${value.name}</div>
                <div>${value.price.toLocaleString()}</div>
                <div>
                    <button onclick="changeQuantity(${key}, ${value.quantity - 1})">-</button>
                    <div class="count">${value.quantity}</div>
                    <button onclick="changeQuantity(${key}, ${value.quantity + 1})">+</button>
                </div>`;
                listCard.appendChild(newDiv);
        }
    })
    total.innerText = totalPrice.toLocaleString();
    quantity.innerText = count;
     console.log('Reload Card Function Called'); // Debug-Ausgabe
}
function changeQuantity(key, quantity){
    if(quantity == 0){
        delete listCards[key];
    }else{
        listCards[key].quantity = quantity;
        listCards[key].price = quantity * products[key].price;
    }
    reloadCard();
}

function submitOrder() {
	// Überprüfen, ob ausgewählte Artikel vorhanden sind
	if (Object.keys(selectedItems).length === 0) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'You must choose at least one item.',
        });
        return; // Das Formular wird nicht abgesendet
    }
    console.log("daten gesendet an der Warenkorb")
    //alert("Thanks we recieve your order")
    // Füge ausgewählte Artikel und Mengen zum Formular hinzu
    const form = document.querySelector('form');
    for (const [itemId, quantity] of Object.entries(selectedItems)) {
        const input = document.createElement('input');
        input.type = 'hidden';
        input.name = `item_${itemId}`;
        input.value = listCards[itemId].quantity;
        form.appendChild(input);
    }
    // Sende das Formular
    form.submit();
    // Zeige eine Bestätigungsnachricht an
    Swal.fire({
        icon: 'success',
        title: 'Thank You!',
        text: 'We have received your order and our waiter will process it as soon as possible.',
    });
}


