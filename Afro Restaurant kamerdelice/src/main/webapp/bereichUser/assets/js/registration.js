/**
 * 
 */
function validatePassword() {
    var password1 = document.getElementById("inputPassword").value;
    var password2 = document.getElementById("confirmPassword").value;

    // Überprüfen, ob die Passwörter übereinstimmen
    if (password1 !== password2) {
        alert("Both Password must be the same.");
        return false;
    }

    // Überprüfen, ob das Passwort die Mindestanforderungen erfüllt
    if (password1.length < 8) {
        alert("Das Passwort muss mindestens 8 Zeichen lang sein.");
        return false;
    }

    if (!/[0-9]/.test(password1)) {
        alert("Password must contain at least one number.ens eine Zahl enthalten.");
        return false;
    }

    if (!/[a-z]/.test(password1) || !/[A-Z]/.test(password1)) {
        alert("Das PasswPassword must contain both lowercase and uppercase letters. \n"
        +" das passwort muss sowohl Kleinbuchstaben als auch Großbuchstaben enthalten.");
        return false;
    }

    // Das Passwort ist gültig
    return true;
}