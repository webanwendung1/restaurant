<%@page import="de.hwg_lu.bw.beans.ReservationBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="rbean" class="de.hwg_lu.bw.beans.ReservationBean" scope="session"></jsp:useBean>

<%!public String denullify(String inputString){
	if (inputString == null) return ""; else return inputString;
//	return (inputString == null)?"":inputString;
}
public String[] denullify(String[] inputStringArray){
	if (inputStringArray == null) return new String[0]; 
	else return inputStringArray;
}%>


<%
//Reservation view 1
String name = request.getParameter("name");
String mail = request.getParameter("email");
String phone = request.getParameter("phone");
String date = request.getParameter("reservation-date");
String heur = request.getParameter("hour");
String nb_personne = request.getParameter("person");
String message = request.getParameter("message");

//recherche des reservation
String email =request.getParameter("email");
String id=request.getParameter("id");
String nom=request.getParameter("nom");

// modifcation und annulaton Ids
String idModif=request.getParameter("idModif");
String idAnnul=request.getParameter("idAnnul");
// achtennnnn
String idSaufveg=request.getParameter("idAnnul");

System.out.println("annuller id " +idAnnul);


//Buttons
String confirmation = this.denullify(request.getParameter("confirmation"));
String modification = this.denullify(request.getParameter("modification"));
String annullation = this.denullify(request.getParameter("annullation"));
String reserver = this.denullify(request.getParameter("Reserver"));

//recherche des reservation buttons..
String recherche = this.denullify(request.getParameter("recherche"));
String Sauvegarder = this.denullify(request.getParameter("Save"));
String Interompre = this.denullify(request.getParameter("Interrupt"));
rbean.setReservationSearh(false);
if (confirmation.equals("Confirmer")){
	System.out.println("Confirmer-Button wurde geklickt");
	
	
	rbean.createReservation();
	
	response.sendRedirect("../jsp/index.jsp");
}
	
else if (modification.equals("Modifier")){
	System.out.println("Modifier-Button wurde geklickt");

	rbean.setReservationZeiger(idModif);
	
	response.sendRedirect("../jsp/reservationAenderView.jsp");
}	
else if (Interompre.equals("Interrupt")){
	System.out.println("Interompre-Button wurde geklickt");

	
	
	response.sendRedirect("../jsp/reservationAendernUndStorniren.jsp");
}	
else if (Sauvegarder.equals("Save")){
	System.out.println("Sauvegarder-Button wurde geklickt");

	rbean.setDate(date);
	rbean.setHeur(heur);
	rbean.setMessage(message);
	rbean.setNb_personne(nb_personne);
	rbean.setName(name);
	rbean.setPhone(phone);
	
	rbean.modificationReservation();
	
	
	response.sendRedirect("../jsp/index.jsp");
}	
else if (reserver.equals("Reserver")){
	System.out.println("Reserver-Button wurde geklickt");
	
	rbean.setDate(date);
	rbean.setHeur(heur);
	rbean.setMessage(message);
	rbean.setNb_personne(nb_personne);
	rbean.setName(name);
	rbean.setMail(mail);
	rbean.setPhone(phone);
	
	response.sendRedirect("../jsp/reserConfirmView.jsp");
}	
else if (annullation.equals("Annuller")){
	System.out.println("Annuller-Button wurde geklickt");
	  
	
		rbean.annullerReservation(idAnnul);
		
	response.sendRedirect("../jsp/reservationAendernUndStorniren.jsp");
	   
	   }
else if (recherche.equals("Search")){
	System.out.println("Recherche-Button wurde geklickt");
	  
	int newId=Integer.parseInt(id);
	
	System.out.println(newId);

		rbean.setEmailRecher(email);
		rbean.setIdRecher(newId);
		rbean.setNomRecher(nom);
		rbean.setReservationSearh(true);
		rbean.recherche();
	response.sendRedirect("../jsp/reservationAendernUndStorniren.jsp");
	   
	   }

else {
	System.out.println("kein Button wurde geklickt");
response.sendRedirect("../jsp/index.jsp");
	
}


%>
</body>
</html>