<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Recherche de réservation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../assets/images/logo.png" type="image/svg+xml">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/form.css">
  	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
   
</head>
<body>

   <%@include file="header.jsp"%>
 
 <div class="bg">
 <br>  <br>  <br>  <br>  <br>  <br> 
 <br>  <br> 
 <div class="containerr">
       <h1>Reservation Search</h1>
		<p><b>Attention!</b> Fill in at least two fields</p>
		<form method="post" action="../jsp/reservationAppl.jsp" style="width: 500px;">
		    <div class="form-group">
		        <label for="id">ID:</label>
		        <input type="text" name="id" id="id" class="form-control" placeholder="Enter reservation ID" required="required" style="height: 40px; font-size: 16px;">
		    </div>
		    <div class="form-group">
		        <label for="nom">Name:</label>
		        <input type="text" name="nom" id="nom" class="form-control" placeholder="Enter customer's name" style="height: 40px; font-size: 16px;">
		    </div>
		    <div class="form-group">
		        <label for="email">Email Address:</label>
		        <input type="email" name="email" id="email" class="form-control" placeholder="Enter customer's email address" style="height: 40px; font-size: 16px;">
		    </div>
		    <button type="submit" class="btn btn-primary" name="recherche" value="Search">Search</button>
		</form>
		<hr>
		<table class="table">
		
		   
                    
                    
                    <!--  resultat de la recherche-->
             <jsp:getProperty property="resultset" name="rbean"/>       
                  
                
                
            </tbody>
        </table>
   
     <p style="color: red; font-size: 22px;  margin-bottom: 300px;">  <jsp:getProperty property="resultNotFound" name="rbean"/></p>       
        
</div></div>
<%@include file="footer.jsp"%>

<script >

$(document).ready(function() {
    // Confirmation function for canceling the reservation
    $(".btn-danger").click(function(event) {
        event.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "The reservation will be canceled permanently!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, cancel!'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = $(this).attr("href");
            }
        });
    });
});


</script>
                                
</body>

</html>