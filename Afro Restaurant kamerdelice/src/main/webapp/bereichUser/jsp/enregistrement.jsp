<%@page import="de.hwg_lu.bw.beans.MessageBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/css/loginReg.css" >
    <title>Login Page</title>
  </head>
  <body>
   <jsp:useBean id="messBean" class="de.hwg_lu.bw.beans.MessageBean" scope="session"></jsp:useBean>
    
    <div class="container mt-5">
      <div class="row">
        <div class="col-md-6 mx-auto">
          <div class="card">
            <div class="card-header">
              <h4>Login</h4>
              <jsp:getProperty property="messageHtml" name="messBean"/>
            </div>
            <div class="card-body">
        <form action="../jsp/loginRegAppl.jsp" method="post">
          <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="emaillogin"  class="form-control" id="username" placeholder="Enter username">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="passlogin" placeholder="Password">
          </div>
                 <button name="simpleLogin" value="simpleLogin" type="submit" class="btn btn-primary btn-block" >Login</button>
                 
                <br> <hr>
                 <button name="adminLogin" value="adminLogin" type="submit" class="btn btn-primary btn-block">Continue as Admin</button> <br><br>
                 <a href="./index.jsp">
                        <button type="button" class="btn btn-secondary btn-block" name="gastlogin" value="Gast">Continue without an account</button>
                        </a>       
   <div > <br> Don't have an account?  <a href="../jsp/connection.jsp">Register here</a></div>
        </form>
         </div>
          </div>
        </div>
      </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
