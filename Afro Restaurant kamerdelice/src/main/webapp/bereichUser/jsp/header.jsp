<%@page import="de.hwg_lu.bw.beans.MessageBean"%>
<%@page import="de.hwg_lu.bw.beans.GetCodeAlsHtml"%>
<%@page import="de.hwg_lu.bw.beans.ReservationBean"%>
<%@page import="de.hwg_lu.bw.beans.LoginBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>KamerDelice </title>
<meta name="title" content="KamerDelice - Amazing & Delicious Food">
<meta name="description" content="This is a Restaurant webapp with html,jsp,css template made by Sophie Saida and Joseph for school">
<!-- 
    - favicon
  -->
  <link rel="shortcut icon" href="../assets/images/logo.png" type="image/svg+xml">

  <!-- 
    - google font link
  -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;700&family=Forum&display=swap" rel="stylesheet">

  <!-- 
    - custom css link
  -->
  <link rel="stylesheet" href="../assets/css/mainStyle.css">

  <!-- 
    - preload images
  -->
  <link rel="preload" as="image" href="../assets/images/hero-slider-1.jpg">
  <link rel="preload" as="image" href="../assets/images/hero-slider-2.jpg">
  <link rel="preload" as="image" href="../assets/images/hero-slider-3.jpg">

</head>

<body id="top">
<jsp:useBean id="loginBean" class="de.hwg_lu.bw.beans.LoginBean"   scope="session"></jsp:useBean>
 <jsp:useBean id="CodeAShtml" class="de.hwg_lu.bw.beans.GetCodeAlsHtml" scope="session"/>
 <jsp:useBean id="messBean" class="de.hwg_lu.bw.beans.MessageBean" scope="session"></jsp:useBean>
 
  <!-- 
    - #PRELOADER
  -->

  <div class="preload" data-preaload>
    <div class="circle"></div>
    <p class="text">Kamer Delice</p>
  </div>





  <!-- 
    - #TOP BAR
  -->

  <div class="topbar">
    <div class="container">

      <address class="topbar-item">
        <div class="icon">
          <ion-icon name="location-outline" aria-hidden="true"></ion-icon>
        </div>

        <span class="span">
          Paradeplatz 23, Mannheim, Badenwüdenberg 35896, DE
        </span>
      </address>

      <div class="separator"></div>

      <div class="topbar-item item-2">
        <div class="icon">
          <ion-icon name="time-outline" aria-hidden="true"></ion-icon>
        </div>

        <span class="span">Daily : 8.00 am to 10.00 pm</span>
      </div>

      <a href="tel:+11234567890" class="topbar-item link">
        <div class="icon">
          <ion-icon name="call-outline" aria-hidden="true"></ion-icon>
        </div>

        <span class="span">+49 123 456 7890</span>
      </a>

      <div class="separator"></div>

      <a href="mailto:kamerdelice.restaurant@gmail.com" class="topbar-item link">
        <div class="icon">
          <ion-icon name="mail-outline" aria-hidden="true"></ion-icon>
        </div>

        <span class="span">kamerdelice.restaurant@gmail.com</span>
      </a>

    </div>
  </div>





  <!-- 
    - #HEADER
  -->

  <header class="header" data-header>
    <div class="container">

      <a href="index.jsp#" class="logo">
        <img src="../assets/images/logo.png" width="100" height="20" alt="KamerDelice - Home">  
      </a>

      <nav class="navbar" data-navbar>

        <button class="close-btn" aria-label="close menu" data-nav-toggler>
          <ion-icon name="close-outline" aria-hidden="true"></ion-icon>
        </button>

        <a href="#" class="logo">
          <img src="../assets/images/logo.png" width="80" height="10" alt="KamerDelice - Home">
        </a>

        <ul class="navbar-list">

          <li class="navbar-item">
            <a href="index.jsp#home" class="navbar-link hover-underline active">
              <div class="separator"></div>

              <span class="span">Home</span>
            </a>
          </li>

          <li class="navbar-item">
            <a href="index.jsp#menu" class="navbar-link hover-underline">
              <div class="separator"></div>

              <span class="span">Menus</span>
            </a>
          </li>

          <li class="navbar-item">
            <a href="index.jsp#about" class="navbar-link hover-underline">
              <div class="separator"></div>

              <span class="span">About Us</span>
            </a>
          </li>

          <li class="navbar-item">
            <a href="index.jsp#chief" class="navbar-link hover-underline">
              <div class="separator"></div>

              <span class="span">Our Chefs</span>
            </a>
          </li>
          <li class="navbar-item">
            <a href="index.jsp#table" class="navbar-link hover-underline">
              <div class="separator"></div>

              <span class="span">Book a table</span>
            </a>
          </li>
          <li class="navbar-item">
            <a href="../jsp/reservationAendernUndStorniren.jsp" class="navbar-link hover-underline">
              <div class="separator"></div>

              <span class="span">manage your Table</span>
            </a>
          </li>
          <li class="navbar-item">
            <a href="../jsp/shopMenus.jsp" class="navbar-link hover-underline">
              <div class="separator"></div>

              <span class="span">Order a dish</span>
            </a>
          </li>

          <li class="navbar-item">
            <a href="index.jsp#table" class="navbar-link hover-underline">
              <div class="separator"></div>

              <span class="span">Contact</span>
            </a>
          </li>

        </ul>

        <div class="text-center">
          <p class="headline-1 navbar-title">Visit Us</p>

          <address class="body-4">
            Paradeplatz 23, Mannheim,  <br>
           Badenwüdenberg 35896, DE
          </address>

          <p class="body-4 navbar-text">Open: 9.30 am - 2.30pm</p>

          <a href="mailto:kamerdelice.restaurant@gmail.com" class="body-4 sidebar-link">kamerdelice.restaurant@gmail.com</a>

          <div class="separator"></div>

          <p class="contact-label">Booking Request</p>

          <a href="tel:+49123123456" class="body-1 contact-number hover-underline">
            +49-123-123456
          </a>
        </div>

      </nav>

      <a href="../jsp/enregistrement.jsp" class="btn btn-secondary">
        <span class="text text-1"> <jsp:getProperty property="loginStatus" name="loginBean"/> </span>

        <span class="text text-2" aria-hidden="true"><jsp:getProperty property="loginStatus" name="loginBean"/></span>
      </a>

      <button class="nav-open-btn" aria-label="open menu" data-nav-toggler>
        <span class="line line-1"></span>
        <span class="line line-2"></span>
        <span class="line line-3"></span>
      </button>

      <div class="overlay" data-nav-toggler data-overlay></div>

    </div>
  </header>

</body>
</html>