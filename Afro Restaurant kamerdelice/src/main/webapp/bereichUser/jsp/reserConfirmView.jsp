<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Recherche de réservation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../assets/images/logo.png" type="image/svg+xml">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/form.css">
  	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
   
</head>
<body>

   <%@include file="header.jsp"%>
 
 <div class="bg">
 <br>  <br>  <br>  <br>  <br>  <br> 
 <br>  <br> 
 <div class="containerr" style="max-width: 1000px;">
       <h1>Confirmation de réservation</h1>
    <p>Merci pour votre réservation, voici les détails :</p>
    <table class="table table2">
       <tbody>
		    <tr>
		        <td class="head">Reservation Number:</td>
		        <td><jsp:getProperty property="zaehler" name="rbean"/> </td>
		    </tr>
		    <tr>
		        <td class="head">Name:</td>
		        <td><jsp:getProperty property="name" name="rbean"/> </td>
		    </tr>
		    <tr>
		        <td class="head">Email Address:</td>
		        <td><jsp:getProperty property="mail" name="rbean"/> </td>
		    </tr>
		    <tr>
		        <td class="head">Date:</td>
		        <td><jsp:getProperty property="date" name="rbean"/> </td>
		    </tr>
		    <tr>
		        <td class="head">Time:</td>
		        <td><jsp:getProperty property="heur" name="rbean"/> </td>
		    </tr>
		    <tr>
		        <td class="head" >Number of People:</td>
		        <td><jsp:getProperty property="nb_personne" name="rbean"/> </td>
		    </tr>
		    <tr>
		        <td class="head">Message to the Restaurant:</td>
		        <td><jsp:getProperty property="message" name="rbean"/> </td>
		    </tr>
		</tbody>

    </table>
    <div class="btons" style=" margin-bottom: 300px;">
    <form action="../jsp/reservationAppl.jsp" method="get">
      <button type="submit" class="btn btn-success" name="confirmation" value="Confirmer">Confirm</button>
       <a href="reservationAppl.jsp?modification=Modifier&idModif=<jsp:getProperty property="zaehler" name="rbean"/>" class="btn btn-sm btn-secondary">Modify</a>
      <a class="btn btn-danger" href="reservationAppl.jsp?annullation=Annuller&idAnnul=<jsp:getProperty property="zaehler" name="rbean"/>" >Cancel</a>
      </form>
</div>
</div>
</div>
<%@include file="footer.jsp"%>

<script >

$(document).ready(function() {
    // Confirmation function for canceling the reservation
    $(".btn-danger").click(function(event) {
        event.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "The reservation will be canceled permanently!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, cancel!'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = $(this).attr("href");
            }
        });
    });
});

</script>
                                
</body>

</html>