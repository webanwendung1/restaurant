<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     
    <title>KamerDelice</title>
    <link rel="stylesheet" href="../assets/css/shopStyle.css">
    <link rel="shortcut icon" href="../assets/images/logo.png" type="image/svg+xml">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
   
</head>
<body >

  <%@ include file="./header.jsp"%>

   <% 
// Überprüfen, ob der Benutzer eingeloggt ist
// Wenn der Benutzer nicht eingeloggt ist, leiten wir ihn zur Login-Seite weiter
// sonst geschieht keine warenkorb-userzuordnung
if (!loginBean.isLoggedIn()) {
    response.sendRedirect("./loginRegAppl.jsp?loginFuerShop=loginFuerShop&message=Bitte melden Sie sich an, um fortzufahren.");
}
%>


      <!-- 
        - #HERO
      -->

      <section class="hero text-center" aria-label="home" id="home">

        <ul class="hero-slider" data-hero-slider>

          <li class="slider-item active" data-hero-slider-item>

            <div class="slider-bg">
              <img src="../assets/images/resto.jpg" width="1880" height="950" alt="" class="img-cover">
            </div>

            <p class="label-2 section-subtitle slider-reveal">Tradational & Hygine</p>

            <h1 class="display-1 hero-title slider-reveal">
             Order for fast Deliverie, <br>
               Pick up or <br>eat your Food in place
            </h1>

            <p class="body-2 hero-text slider-reveal">
              Come with family & feel the joy of mouthwatering food
            </p>

            <a href="#body"" class="btn btn-primary slider-reveal">
              <span class="text text-1">View Our Menu</span>

              <span class="text text-2" aria-hidden="true">View Our Menu</span>
            </a>

          </li>

          <li class="slider-item" data-hero-slider-item>

            <div class="slider-bg">
              <img src="../assets/images/background2.jpg" width="1880" height="950" alt="" class="img-cover">
            </div>

            <p class="label-2 section-subtitle slider-reveal">delightful experience</p>

            <h1 class="display-1 hero-title slider-reveal">
             Discover something rustic <br>
             from Cameroonian cuisine
            </h1>

            <p class="body-2 hero-text slider-reveal">
              Come with family & feel the joy of mouthwatering food
            </p>

            <a href="#body"" class="btn btn-primary slider-reveal">
              <span class="text text-1">View Our Menu</span>

              <span class="text text-2" aria-hidden="true">View Our Menu</span>
            </a>

          </li>

          <li class="slider-item" data-hero-slider-item>

            <div class="slider-bg">
              <img src="../assets/images/hero-slider-3.jpg" width="1880" height="950" alt="" class="img-cover">
            </div>

            <p class="label-2 section-subtitle slider-reveal">amazing & delicious</p>

            <h1 class="display-1 hero-title slider-reveal">
              Where every flavor <br>
              tells a story
            </h1>

            <p class="body-2 hero-text slider-reveal">
              Come with family & feel the joy of mouthwatering food
            </p>

            <a href="#body" class="btn btn-primary slider-reveal">
              <span class="text text-1">View Our Menu</span>

              <span class="text text-2" aria-hidden="true">View Our Menu</span>
            </a>

          </li>

        </ul>

        <button class="slider-btn prev" aria-label="slide to previous" data-prev-btn>
          <ion-icon name="chevron-back"></ion-icon>
        </button>

        <button class="slider-btn next" aria-label="slide to next" data-next-btn>
          <ion-icon name="chevron-forward"></ion-icon>
        </button>

        <a href="index.jsp#contact" class="hero-btn has-after">
          <img src="../assets/images/hero-icon.png" width="48" height="48" alt="booking icon">

          <span class="label-2 text-center span">Book A Table</span>
        </a>

      </section>

<div class="body" id="body">
    <div class="containers">
        <div class="shopping-navbar">
    <div class="headers">
    
        <h1>Your Shopping Cart</h1>
        <div class="categories">
        <select name="categorySelect" class="categorieswerte" id="categorySelect"  onchange="filterByCategory()" style=" color:white ;font-size: 32px;font-weight: bold;">
            <option value="all"  style="background-color: #414141;">All Categories</option>
            <option value="Vorspeisen" style="background-color: #414141;">Vorspeisen</option>
            <option value="Hauptspeisen" style="background-color: #414141;">Hauptspeisen</option>
            <option value="Beilagen" style="background-color: #414141;">Beilagen</option>
            <option value="Spezialitaet" style="background-color: #414141;">Spezialitaet</option>
            <option value="Getraenke " style="background-color: #414141;">Getraenke </option>
            <option value="Nachttisch " style="background-color: #414141;">Nachttisch </option>
        </select>
    </div>
        <div class="shopping">
            <img src="../assets/images/bag.png">
            <span class="quantity">0</span>
        </div>
    </div>
    
    
</div>


        <div class="list">
        
           <img src="../assets/images/shape-7.png" width="208" height="178" loading="lazy" alt="shape"
            class="shape shape-1">

          <img src="../assets/images/shape-8.png" width="120" height="115" loading="lazy" alt="shape"
            class="shape shape-2">
        	<img src="../assets/images/shape-5.png" width="921" height="1036" loading="lazy" alt="shape"
            class="shape shape-2 move-anim">
          <img src="../assets/images/shape-6.png" width="343" height="345" loading="lazy" alt="shape"
            class="shape shape-3 move-anim">
        
        </div>
    </div>
     
    
     <form action="shopAppl.jsp" method="post">
    <div class="card">
        
        <table style="width: 100%;">
  
		  <tr>
		    <td><h1>Card </h1></td>
		    <td><button type="button" class="btn btn-secondary" style="width: 100%; margin-left: 0; color: white;" onclick="submitOrder()"> <span class="text">Order Now</span> </button></td>
		  </tr>
		</table>

        <ul class="listCard">
        </ul>
        <div class="checkOut" style="bottom: 0; margin-top: 10px;">
        
            <div style="color: white;" class="total btn1">0</div>
            <div class="closeShopping btn1">Close</div>
            
        </div>
    </div>
</form>



 <img src="../assets/images/shape-7.png" width="208" height="178" loading="lazy" alt="shape"
            class="shape shape-1">

          <img src="../assets/images/shape-8.png" width="120" height="115" loading="lazy" alt="shape"
            class="shape shape-2">
        	<img src="../assets/images/shape-5.png" width="921" height="1036" loading="lazy" alt="shape"
            class="shape shape-2 move-anim">
          <img src="../assets/images/shape-6.png" width="343" height="345" loading="lazy" alt="shape"
            class="shape shape-3 move-anim">
        
</div>
 <%@ include file="./footer.jsp"%>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.all.min.js"></script>
<script src="../assets/js/app.js"></script>
</body>
</html>