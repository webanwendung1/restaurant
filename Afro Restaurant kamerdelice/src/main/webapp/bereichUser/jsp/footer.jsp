<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


  <!-- 
    - #FOOTER
  -->

  <footer class="footer section has-bg-image text-center"
    style="background-image: url('../assets/images/footer-bg.jpg')">
    <div class="container">

      <div class="footer-top grid-list">

        <div class="footer-brand has-before has-after">

          <a href="#" class="logo">
            <img src="../assets/images/logo.png" width="160" height="50" loading="lazy" alt="grilli home">
          </a>

          <address class="body-4">
           Paradeplatz 23, Mannheim, Badenwüdenberg 35896, DE
          </address>

          <a href="mailto:booking@grilli.com" class="body-4 contact-link">kamerdelice.restaurant@gmail.com</a>

          <a href="tel:+49 123123456" class="body-4 contact-link">Booking Request : +49-123-123456</a>

          <p class="body-4">
            Open : 08:00 am - 10:00 pm
          </p>

          <div class="wrapper">
            <div class="separator"></div>
            <div class="separator"></div>
            <div class="separator"></div>
          </div>

          <p class="title-1">Get News & Offers</p>

          <p class="label-1">
            Subscribe us & Get <span class="span">25% Off.</span>
          </p>

          <form action="" class="input-wrapper">
            <div class="icon-wrapper">
              <ion-icon name="mail-outline" aria-hidden="true"></ion-icon>

              <input type="email" name="email_address" placeholder="Your email" autocomplete="off" class="input-field">
            </div>

            <button type="submit" class="btn btn-secondary">
              <span class="text text-1">Subscribe</span>

              <span class="text text-2" aria-hidden="true">Subscribe</span>
            </button>
          </form>

        </div>

        <ul class="footer-list">

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Home</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Menus</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">About Us</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Our Chefs</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Contact</a>
          </li>

        </ul>

        <ul class="footer-list">

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Facebook</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Instagram</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Twitter</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Youtube</a>
          </li>

          <li>
            <a href="#" class="label-2 footer-link hover-underline">Google Map</a>
          </li>

        </ul>

      </div>
      
<div class="footer-container" id="bottom">

			<div class="footer-hours">

				<h3>oeffnungzeiten</h3>

				<p>Montag-Sonntag</p>

				<p>08:00 am bis 10:00 pm</p>

				<p>Montag (Sonder Angeboten)</p>

			</div>

			<div class="footer-contact">

				<h3>Kontakt</h3>

				<p>Telephone : 0176 23 45 67 89</p>

				<p>Email : kamerdelice.restaurant@gmail.com</p>
			</div>

			<div class="footer-address">
				<h3>Anschrift</h3>
				<p>C2,16-18</p>
				<p>Mannheim</p>

				<p>Kmer-Delices</p>
			</div>
		</div>
		
		<br>
		<br>
		<br>
      <div class="footer-bottom">

        <p class="copyright">
          &copy; 2023 KamerDelice. All Rights Reserved | Crafted by <a href="https://github.com/sophiealida"
            target="_blank" class="link">Sophie, Saida and Joseph</a>
        </p>

      </div>

    </div>
  </footer>
 





  <!-- 
    - #BACK TO TOP
  -->

  <a href="#top" class="back-top-btn active" aria-label="back to top" data-back-top-btn>
    <ion-icon name="chevron-up" aria-hidden="true"></ion-icon>
  </a>
  
  <a href="#bottom" class="back-down-btn active" aria-label="back to down" data-back-down-btn>
    <ion-icon name="chevron-down" aria-hidden="true"></ion-icon>
  </a>





<!-- Chargement des fichiers JavaScript -->
  <!-- 
    - custom js link
  -->
  <script src="../assets/js/script.js"></script>

  <!-- 
    - ionicon link
  -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
 
</body>
</html>