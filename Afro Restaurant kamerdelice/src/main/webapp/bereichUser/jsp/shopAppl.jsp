<%@page import="de.hwg_lu.bw.beans.LoginBean"%>
<%@page import="de.hwg_lu.bw.beans.WarenKorb"%>
<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="wk" class="de.hwg_lu.bw.beans.WarenKorb"
		scope="session"></jsp:useBean>
	<jsp:useBean id="loginBean" class="de.hwg_lu.bw.beans.LoginBean"
		scope="session"></jsp:useBean>
	<jsp:useBean id="EmailSender" class="de.hwg_lu.bw.beans.EmailSender"
		scope="session"></jsp:useBean>
	<%
	// Annahme: Die versteckten Input-Felder haben Namen im Format "item_x", wobei "x" die Artikel-ID ist.
	// Rufe alle Parameter aus dem Request ab
	Enumeration<String> parameterNames = request.getParameterNames();
	String body = " Sehr Geeherte(r) kundeIn, \n\nVielen Danke f�r Ihre Bestellung.\n"
			+"sie haben fogende Items bestellt:\n\n";
	// Iterieren Sie durch die Parameter und pr�fen Sie, ob sie mit "item_" beginnen
	while (parameterNames.hasMoreElements()) {

		wk.setUserId(loginBean.getUser_id());
		String paramName = parameterNames.nextElement();
		if (paramName.startsWith("item_")) {
			String itemId = paramName.substring("item_".length()); // Extrahieren Sie die Artikel-ID
			String quantity = request.getParameter(paramName); // Die Menge des Artikels
			System.out.println("quantity " + quantity);
			int product_id = Integer.parseInt(itemId);
			int quantityInt = Integer.parseInt(quantity);
			// Hier werden "itemId" und "quantity"  in die Datenbank speichern
			wk.addProdukt(product_id, quantityInt);
			body += quantity + " mal produkt mit Id " + product_id + ",\n";
			System.out.println("ProductId " + product_id + " mit quantity " + quantity
			+ " zum Warekorb send for user mit id " + loginBean.getUser_id());

		}

	}
	wk.saveToDatabase();
	EmailSender.setTo(loginBean.getMail());
	EmailSender.setSujet("Order Corfirmatiom ");
	EmailSender.setBodyMail(body
			+ " \n\n wir k�mmern uns umgehen darum. \n Sie bekommen von einer unser  Kellner die rechenung\n\n"
			+"Sie k�nnen bar oder mit Karte bezahlen\n Mit freundlichen Gr�ssen\n Kamer Delice");
	EmailSender.sendMail();
	response.sendRedirect("../jsp/index.jsp");
	%>
</body>
</html>