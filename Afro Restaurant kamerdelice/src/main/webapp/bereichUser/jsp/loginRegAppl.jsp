<%@page import="de.hwg_lu.bw.beans.MessageBean"%>
<%@page import="de.hwg_lu.bw.beans.AccountBean"%>
<%@page import="de.hwg_lu.bw.beans.GetCodeAlsHtml"%>
<%@page import="de.hwg_lu.bw.beans.LoginBean"%>
<%@page import="java.sql.SQLException"%>
<%@page import="de.hwg_lu.bw.jdbc.NoConnectionException"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="accountBean" class="de.hwg_lu.bw.beans.AccountBean" scope="session"></jsp:useBean>
<jsp:useBean id="loginBean" class="de.hwg_lu.bw.beans.LoginBean" scope="session"></jsp:useBean>
<jsp:useBean id="htmlexprexion" class="de.hwg_lu.bw.beans.GetCodeAlsHtml" scope="session"></jsp:useBean>
<jsp:useBean id="messBean" class="de.hwg_lu.bw.beans.MessageBean" scope="session"></jsp:useBean>

<%!
public String denullify(String inputString){
	if (inputString == null) return ""; else return inputString;
//	return (inputString == null)?"":inputString;
}
public String[] denullify(String[] inputStringArray){
	if (inputStringArray == null) return new String[0]; else return inputStringArray;
}
%>
	<%
//Eingabefelder

String emailLogin = request.getParameter("emaillogin");
String passwordlogin = request.getParameter("passlogin");
String password = request.getParameter("pass");
String password2 = request.getParameter("pass2");
String email = request.getParameter("email");
String name = request.getParameter("name");
String message = request.getParameter("message");







String adminLogin = this.denullify(request.getParameter("adminLogin"));
String simpleLogin = this.denullify(request.getParameter("simpleLogin"));
String registration = this.denullify(request.getParameter("registration"));
String gastlogin = this.denullify(request.getParameter("gastlogin"));
String loginFuerShop = this.denullify(request.getParameter("loginFuerShop"));


 if (registration.equals("Registrieren")){
	System.out.println("Registrieren-Button wurde geklickt");

	
	System.out.println("pass gleich durch script");
	accountBean.setPassword(password);
	accountBean.setEmail(email);
	accountBean.setActive("Y");
	accountBean.setAdmin("N");
	accountBean.setUsername(name);

	try{
		boolean insertOk = accountBean.insertAccountIfNotExists();
		if (insertOk){//Nickname wurde geschrieben
			messBean.setAccountCreated(name);
			response.sendRedirect("../jsp/enregistrement.jsp");	
		}else{
			messBean.setAccountAlreadyExists(email);
			response.sendRedirect("../jsp/connection.jsp");	
		}
	}catch(NoConnectionException exc){
		exc.printStackTrace();
		messBean.setDBError();
		response.sendRedirect("../jsp/connection.jsp");	
	}catch(SQLException exc){
		exc.printStackTrace();
		messBean.setSQLError();
		response.sendRedirect("../jsp/connection.jsp");	
	}
}

else if (adminLogin.equals("adminLogin")){
	
	loginBean.setMail(emailLogin);
	loginBean.setPassword(passwordlogin);
	
	
	System.out.println(" versuch einzulogen als Admin eingelietet");
	try{
		boolean useridPasswordOk = loginBean.checkUseridPassword();
		boolean admin = loginBean.getAdmin();
		if (useridPasswordOk & admin){
			messBean.setInfoMessage("");
			messBean.setActionMessage("");
			loginBean.setLoggedIn(true);
			loginBean.setAdmin(true);
			System.out.println(" versuch einzulogen erfolgreich");
			response.sendRedirect("../../bereichAdmin/jsp/index.jsp");
		}else{
			messBean.setLoginFailed();
			loginBean.setLoggedIn(false);
			System.out.println(" versuch einzulogen fehlgesclagen");
			response.sendRedirect("../jsp/enregistrement.jsp");
		}
	}catch(NoConnectionException nce){
		messBean.setDBError();
		response.sendRedirect("../jsp/./enregistrement.jsp");
	}catch(SQLException se){
		messBean.setSQLError();
		response.sendRedirect("../jsp/enregistrement.jsp");
	}
}
else if (simpleLogin.equals("simpleLogin")){
	
	loginBean.setMail(emailLogin);
	loginBean.setPassword(passwordlogin);
	
	
	System.out.println(" versuch simpleLogin eingelietet");
	try{
		boolean useridPasswordOk = loginBean.checkUseridPassword();
		 
		if (useridPasswordOk){
			messBean.setInfoMessage("");
			messBean.setActionMessage("");
			loginBean.setLoggedIn(true);
			loginBean.setAdmin(false);

			System.out.println(" versuch einzulogen erfolgreich");
			if(loginBean.isLoginSchop())				
			response.sendRedirect("../jsp/shopMenus.jsp#body");
			else if(loginBean.isLoginFuerAdmin())				
			response.sendRedirect("../../bereichAdmin/jsp/index.jsp");
			else
			response.sendRedirect("../jsp/index.jsp");
		}else{
		    messBean.setLoginFailed();
			loginBean.setLoggedIn(false);
			System.out.println(" versuch einzulogen fehlgesclagen");
			response.sendRedirect("../jsp/enregistrement.jsp");
		}
	}catch(NoConnectionException nce){
		messBean.setSQLError();
		response.sendRedirect("../jsp/enregistrement.jsp");
	}catch(SQLException se){
		messBean.setDBError();
		response.sendRedirect("../jsp/enregistrement.jsp");
	}
}
else if (gastlogin.equals("Gast")){
	
	
	 
	
	System.out.println(" Gast versuch einzulogen eingelietet");
	messBean.setInfoMessage("");
	messBean.setActionMessage("");
	loginBean.setLoggedIn(false);
	loginBean.setAdmin(false);
	loginBean.setLoginSchop(false);
	

	response.sendRedirect("../jsp/index.jsp");
	
}
else if (loginFuerShop.equals("loginFuerShop")){
	
	System.out.println(" loginFuerShop versuch einzulogen eingelietet");
	
	messBean.setInfoMessage(message);
	messBean.setActionMessage("Geben sie ihre daten ein oder registrieren sie sich");
	loginBean.setLoggedIn(false);
	loginBean.setAdmin(false);
	loginBean.setLoginSchop(true);
	response.sendRedirect("../jsp/enregistrement.jsp");
	
}
else if (loginFuerShop.equals("loginFuerAdmin")){
	
	System.out.println(" loginFuerAdmin versuch einzulogen eingelietet");
	
	messBean.setInfoMessage(message);
	messBean.setActionMessage("Geben sie ihre daten ein oder registrieren sie sich");
	loginBean.setLoggedIn(false);
	loginBean.setAdmin(false);
	loginBean.setLoginFuerAdmin(true);
	response.sendRedirect("../jsp/enregistrement.jsp");
	
}

else{
	System.out.println("kein Button gedr�ckt auch ");
	//messageBean.setGeneralWelcome();
	accountBean.setAccoutcheck(false);	
	loginBean.setAdmin(false);
	messBean.setInfoMessage("");
	messBean.setActionMessage("");
	loginBean.setLoggedIn(false);
	loginBean.setAdmin(false);
	loginBean.setLoginSchop(false);
	response.sendRedirect("../jsp/index.jsp");
}


%>
</body>
</html>