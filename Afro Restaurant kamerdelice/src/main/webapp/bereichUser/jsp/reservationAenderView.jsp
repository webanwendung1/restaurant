<%@page import="de.hwg_lu.bw.beans.ReservationBean"%>
<%@page import="de.hwg_lu.bw.beans.GetCodeAlsHtml"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Réservation de restaurant</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/form.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
   
    
</head>
<body>
 <%@include file="header.jsp"%>
 <div class="bg">
    <br>  <br>  <br>  <br>  <br>  <br> 
    <br>  <br> 
    <div class="warp containerr">
        <h1>Modify Reservation</h1>
        <form action="../jsp/reservationAppl.jsp" method="post" style="width: 500px;">
            <div class="form-group">
                <label for="reservationNumber">Reservation Number:</label>
                <input type="text" class="form-control" id="reservationNumber" name="reservationNumber" value="<jsp:getProperty property="zaehler" name="rbean"/>" disabled="disabled" title="can't be edit" style="height: 40px; font-size: 16px;">
            </div>
            <div class="form-group">
                <label for=name>Reservation Name:</label>
                <input type="text" class="form-control" id="name" name="name" value="<jsp:getProperty property="name" name="rbean"/>"   style="height: 40px; font-size: 16px;">
            </div>
            <div class="form-group">
                <label for="email">Your Email Address:</label>
                <input type="email" class="form-control" id="email" name="email" value="<jsp:getProperty property="mail" name="rbean"/>" disabled="disabled" title="can't be edit" style="height: 40px; font-size: 16px;">
            </div>
            <div class="form-group">
                <label for="date">Date:</label>
                <input type="text" class="form-control" id="date" name="reservation-date" value="<jsp:getProperty property="date" name="rbean"/> " style="height: 40px; font-size: 16px;">
            </div>
            <div class="form-group">
                <label for="time">Time:</label>
                <input type="text" class="form-control" id="time" name="hour" value="<jsp:getProperty property="heur" name="rbean"/>" style="height: 40px; font-size: 16px;">
            </div>
            <div class="form-group">
                <label for="numOfPeople">Number of People:</label>
                <input type="text" class="form-control" id="numOfPeople" name="person" min="1" max="20" value="<jsp:getProperty property="nb_personne" name="rbean"/>" style="height: 40px; font-size: 16px;">
            </div>
            <div class="form-group">
                <label for="restaurantMessage">Message to the Restaurant:</label>
                <textarea class="textarea form-control" rows="6" name="message" id="restaurantMessage" style="font-size: 16px;"><jsp:getProperty property="message" name="rbean" /></textarea>
            </div>
            <button type="submit" class="btn btn-primary" name="Interrupt" value="Interrupt">Interrupt</button>
            <button type="submit" class="btn btn-success"  name="Save" value="Save" id="">Save</button>
        </form>
        <hr>
        <p>Return to the homepage, <a class="link" href="../html/index.jsp"  style="color: blue; font-size: 16px" > click here! </a> </p>
    </div>
</div>

<%@include file="footer.jsp"%>
    <script type="text/javascript">
    
function replace() {
	document.getElementById("ph").innerHTML = "";
}


$(document).ready(function() {
    // Confirmation function for canceling the reservation
    $(".btn-primary").click(function(event) {
        event.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "The reservation will be saved permanently!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, Intrtupt!'
        }).then((result) => {
            if (result.isConfirmed) {
                // Submit the form associated with the clicked button
                $(this).closest('form').submit();
            }
        });
    });
});
</script>

</body>
</html>
