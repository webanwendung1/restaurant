
<%@page import="de.hwg_lu.bw.beans.MessageBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/loginReg.css" >
    <title>Enregistrement</title>
  </head>
  <body>

       <jsp:useBean id="messBean" class="de.hwg_lu.bw.beans.MessageBean"></jsp:useBean>
    
    
  
  <div class="container mt-5">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4>Registration</h4>
                                  <jsp:getProperty property="messageHtml" name="messBean"/>
                    
                </div>
                <div class="card-body">
                    <form action="../jsp/loginRegAppl.jsp" method="get"  onsubmit="return validatePassword();">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" name="name" class="form-control" id="inputName" placeholder="Enter your name">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email Address</label>
                            <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Enter your email address">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" name="pass" class="form-control" id="inputPassword" placeholder="Enter a password">
                        </div>
                        <div class="form-group">
                            <label for="confirmPassword">Confirm Password</label>
                            <input type="password" name="pass2" class="form-control" id="confirmPassword" placeholder="Confirm the password">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" name="registration" value="Registrieren">Register</button>
                        <br> <hr>
                        <a href="./index.jsp">
                        <button type="button" class="btn btn-primary btn-block" name="gastlogin" value="Gast">Continue without an account</button>
                        </a>
                        <div> <br> Already have an account? <a href="enregistrement.jsp">Log in here</a></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


   <script src="../assets/js/registration.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 
  </body>
</html>
