<%@page import="de.hwg_lu.bw.beans.ReportingBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- Font Awesome CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
	<!-- My CSS -->
	<link rel="stylesheet" href="../css/style.css">
  <link rel="shortcut icon" href="../assets/images/logo.png" type="image/svg+xml">

	<title>AdminReporting</title>
</head>
<body>
<jsp:useBean id="reportingBean" class="de.hwg_lu.bw.beans.ReportingBean"></jsp:useBean>
<jsp:useBean id="accountBean" class="de.hwg_lu.bw.beans.AccountBean" scope="session"></jsp:useBean>
<jsp:useBean id="loginBean" class="de.hwg_lu.bw.beans.LoginBean" scope="session"></jsp:useBean>
<jsp:useBean id="messBean" class="de.hwg_lu.bw.beans.MessageBean" scope="session"></jsp:useBean>

	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="#" class="brand">
			<i class='bx bxs-smile'></i>
			<span class="text">AdminReporting</span>
		</a>
		<ul class="side-menu top">
			<li class="active">
				<a href="#">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="../../bereichUser/jsp/index.jsp">
					<i class='bx bxs-shopping-bag-alt' ></i>
					<span class="text">My Store</span>
				</a>
			</li>
			<li>
				<a href="#">
					<i class='bx bxs-doughnut-chart' ></i>
					<span class="text">Analytics</span>
				</a>
			</li>
			<li>
				<a href="#">
					<i class='bx bxs-message-dots' ></i>
					<span class="text">Message</span>
				</a>
			</li>
			<li>
				<a href="#">
					<i class='bx bxs-group' ></i>
					<span class="text">Team</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
			<li>
				<a href="#">
					<i class='bx bxs-cog' ></i>
					<span class="text">Settings</span>
				</a>
			</li>
			<li>
				<a href="../../bereichUser/jsp/enregistrement.jsp" class="logout">
					<i class='bx bxs-log-out-circle' ></i>
					<span class="text">Logout</span>
				</a>
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->



	<!-- CONTENT -->
	<section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form action="#">
				<div class="form-input">
					<input type="search" placeholder="Search...">
					<button type="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
			<input type="checkbox" id="switch-mode" hidden>
			<label for="switch-mode" class="switch-mode"></label>
			<a href="#" class="notification">
				<i class='bx bxs-bell' ></i>
				<span class="num">28</span>
			</a>
			<a href="#" class="profile">
				<img src="../img/people.jpg">
			</a>
		</nav>
		<!-- NAVBAR -->

		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Dashboard</h1>
					<ul class="breadcrumb">
						<li>
							<a href="#">Dashboard</a>
						</li>
						<li><i class='bx bx-chevron-right' ></i></li>
						<li>
							<a class="active" href="#">Home</a>
						</li>
					</ul>
				</div>
				<a href="#" class="btn-download" onclick='window.location.reload();'>
					<i class="fas fa-sync" ></i>
					<span class="text">Refresh Page</span>
				</a>
			</div>

			<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3><jsp:getProperty property="anzahlPendingBestellungen" name="reportingBean"/> </h3>
						<p>New Order</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3><jsp:getProperty property="anzahlKunde" name="reportingBean"/> </h3>
						<p>Visitors</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3>$<jsp:getProperty property="gesamtumsatz" name="reportingBean"/> </h3>
						<p>Total Sales</p>
					</span>
				</li>
			</ul>


			<div class="table-data">
				<div class="order">
					<div class="head">
						<h3>Recent Orders</h3>
						<i class='bx bx-search' ></i>
						<i class='bx bx-filter' ></i>
					</div>
					<table>
						<thead>
							<tr>
								<th>User</th>
								<th>Date Order</th>
								<th>Total Amound</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						<jsp:getProperty property="kennzalFromDB" name="reportingBean"/> 
							<tr>
								<td>
									<img src="../img/people.png">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td>$25,4</td>
								<td><span class="status completed">Completed</span></td>
							</tr>
							<tr>
								<td>
									<img src="../img/people.jpg">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td>$45,84</td>
								<td><span class="status pending">Pending</span></td>
							</tr>
							<tr>
								<td>
									<img src="../img/people.png">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td>$85,4</td>
								<td><span class="status process">Process</span></td>
							</tr>
							<tr>
								<td>
									<img src="../img/people.jpg">
									<p>Sophie Alida</p>
								</td>
								<td>01-10-2021</td>
								<td>$26,77</td>
								<td><span class="status pending">Pending</span></td>
							</tr>
							<tr>
								<td>
									<img src="../img/people.png">
									<p>Saida Leukam</p>
								</td>
								<td>01-10-2021</td>
								<td>$25,4</td>
								<td><span class="status completed">Completed</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="todo">		
				<div class="order">
					<div class="head">
						<h3>Open Reservations</h3>
						<i class='bx bx-search' ></i>
						<i class='bx bx-filter' ></i>
					</div>
					<table>
						<thead>			
							<tr>
								<th>cleint Name</th>
								<th>Date </th>
								<th>Nomber of Person</th>
								<th>Time</th>
								
							</tr>
						</thead>
						<tbody>
						<jsp:getProperty property="reservations" name="reportingBean"/> 
						
							<tr>
								<td>
									<p>Miss Saida</p>
								</td>
								<td>14-09-2023</td>
								<td>2 Personn</td>
								<td>12:30 AM</td>
							</tr>
							<tr>
								<td>								
									<p>Boss Joseph</p>
								</td>
								<td>30-10-2024</td>
								<td>9 Personn</td>
								<td>1:30 PM</td>
							</tr>
							<tr>
								<td>
									<p>Miss Sophie</p>
								</td>
								<td>01-09-2023</td>
								<td>3 Personn</td>
								<td>12:30 PM</td>
							</tr>
								
						</tbody>
					</table>
				</div> 
				<br><br><br>
					<div class="head">
						<h3>Todos</h3>
						<i class='bx bx-plus' ></i>
						<i class='bx bx-filter' ></i>
					</div>
					<ul class="todo-list">
						<li class="completed">
							<p>Buy ries for next week</p>
							<i class='bx bx-dots-vertical-rounded' ></i>
						</li>
						<li class="completed">
							<p>Employees weekly Meeting</p>
							<i class='bx bx-dots-vertical-rounded' ></i>
						</li>
						<li class="not-completed">
							<p>Reparation of the brocken Table</p>
							<i class='bx bx-dots-vertical-rounded' ></i>
						</li>
						<li class="completed">
							<p>Design a new menus cart</p>
							<i class='bx bx-dots-vertical-rounded' ></i>
						</li>
						<li class="not-completed">
							<p>Bank Appointment for more financement</p>
							<i class='bx bx-dots-vertical-rounded' ></i>
						</li>
					</ul>
				</div>
			</div>
		</main>
		<!-- MAIN -->
	</section>
	<!-- CONTENT -->
	
  <% 
// Überprüfen, ob der Benutzer eingeloggt ist
// Wenn der Benutzer nicht eingeloggt ist, leiten wir ihn zur Login-Seite weiter
// sonst geschieht keine warenkorb-userzuordnung
if (!loginBean.isLoggedIn()&!loginBean.isAdmin()) {
    response.sendRedirect("../../bereichUser/jsp/loginRegAppl.jsp?loginFuerShop=loginFuerAdmin&message=Bitte melden Sie sich an, um als Admin fortzufahren.");
}%>
	<script src="../js/script.js"></script>
</body>
</html>